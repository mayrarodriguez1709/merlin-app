import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CartoonRoutingModule } from './cartoon-routing.module';
import { CartoonComponent } from './cartoon.component';
import { CartoonItemComponent } from './cartoon-item/cartoon-item.component';

@NgModule({
  declarations: [CartoonComponent, CartoonItemComponent],
  imports: [
    CommonModule,
    CartoonRoutingModule
  ]
})
export class CartoonModule { }
