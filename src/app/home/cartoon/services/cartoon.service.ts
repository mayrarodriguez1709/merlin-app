import { Injectable } from '@angular/core';
import { ApiService } from '../../../core/services/api/api.service';

@Injectable({
  providedIn: 'root'
})
export class CartoonService {

  private readonly endpoint = 'character';

  constructor(
    private apiService: ApiService
  ) { }

  getAll() {
    return this.apiService.get(`${this.endpoint}/`);
  }

  getAllRandom(randomNumbers: number) {
    const randomNumbArray = this.getRandomArray(randomNumbers);
    return this.apiService.get(`${this.endpoint}/${randomNumbArray}`);
  }

  private getRandomArray(randomNumbers: number) {
    const array = [];

    for (let i = 0; i < randomNumbers; i ++) {
      const random = Math.floor(Math.random() * 300) + 1;
      array.push(random);
    }

    return array;
  }
}
