import { Component, OnInit } from '@angular/core';
import { CartoonService } from './services/cartoon.service';
import { CartoonModel } from '../../core/models/cartoon.model';

@Component({
  selector: 'app-cartoon',
  templateUrl: './cartoon.component.html',
  styleUrls: ['./cartoon.component.scss']
})
export class CartoonComponent implements OnInit {

  cartoons: CartoonModel[] = [];

  constructor(
    private cartoonService: CartoonService
  ) { }

  ngOnInit() {
    this.getAllCartoons();
  }

  getAllCartoons() {
    this.cartoonService.getAllRandom(20)
      .subscribe((res: any) => this.cartoons = res);
  }
}
