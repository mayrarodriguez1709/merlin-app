import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartoonComponent } from './cartoon.component';

const routes: Routes = [
  {
    path: '',
    component: CartoonComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartoonRoutingModule { }
