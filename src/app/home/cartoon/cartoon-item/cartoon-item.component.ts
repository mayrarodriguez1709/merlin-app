import { Component, Input, OnInit } from '@angular/core';
import { CartoonModel } from '../../../core/models/cartoon.model';

@Component({
  selector: 'app-cartoon-item',
  templateUrl: './cartoon-item.component.html',
  styleUrls: ['./cartoon-item.component.scss']
})
export class CartoonItemComponent implements OnInit {
  @Input()
  cartoon: CartoonModel;

  constructor() { }

  ngOnInit() {
  }

}
