import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartoonItemComponent } from './cartoon-item.component';

describe('CartoonItemComponent', () => {
  let component: CartoonItemComponent;
  let fixture: ComponentFixture<CartoonItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartoonItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartoonItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
