import { Injectable } from '@angular/core';
import { LocalStorageService } from '../local-storage/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthStorageService {

  private role: string;
  private token: string;
  private fullName: string;
  private userId: string;
  readonly tokenKey = 'token';

  constructor(
    private storage: LocalStorageService,
  ) {
  }

  clean(): void {
    this.fullName = this.token = this.role = undefined;
    this.storage.clear();
  }

  setRole(role: string): void {
    this.role = role;
  }

  getRole = (): string => this.role;

  setToken(token: string): void {
    // console.log('token saved! ', token);
    this.token = token;
    try {
      this.storage.store(this.tokenKey, token);
    } catch (e) {
      console.log(e)
    }
  }

  getToken(): string {
    return this.storage.retrieve(this.tokenKey);
  }

  setFullName(fullName: string): void {
    this.fullName = fullName;
  }

  getFullName = (): string => this.fullName;

  setUserId(userId: string): void {
    // console.log('userId saved! ', userId);
    this.userId = userId;
  }

  getUserId = (): string => this.userId;
}
