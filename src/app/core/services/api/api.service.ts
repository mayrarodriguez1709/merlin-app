import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { RequestService } from '../request/request.service';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  readonly apiUrl: string;

  constructor(
    protected http: HttpClient,
    private router: Router,
    private request: RequestService,
  ) {
    this.apiUrl = environment.apiUrl;
  }

  public delete<T>(url: string, options?: any, withTokenHeader = false): Observable<T | ArrayBuffer>  {
    const headers = withTokenHeader ? this.request.getBearerHeader() : this.request.getJsonHeader();
    options = { headers, ...options };

    return this.http.delete(`${this.apiUrl}${url}`, options)
      .pipe(
        map(this.extractData),
        catchError(e => throwError(e))
      );
  }

  public get<T>(url: string, options?: any, withTokenHeader = false): Observable<T> {
    const headers = withTokenHeader ? this.request.getBearerHeader() : this.request.getJsonHeader();
    options = { headers, ...options };

    return this.http.get(`${this.apiUrl}${url}`)
      .pipe(
        map(this.extractData),
        catchError(e => throwError(e))
      );
  }

  public patch<T>(url: string, body: any, options?: any, withTokenHeader = false): Observable<T> {
    const headers = withTokenHeader ? this.request.getBearerHeader() : this.request.getJsonHeader();
    options = { headers, ...options };

    return this.http.patch(`${this.apiUrl}${url}`, body, options)
      .pipe(
        map(this.extractData),
        catchError(e => throwError(e))
      );
  }

  public post<T>(url: string, body: any, options?: any, withTokenHeader = false): Observable<T> {
    const headers = withTokenHeader ? this.request.getBearerHeader() : this.request.getJsonHeader();
    options = { headers, ...options };

    return this.http.post(`${this.apiUrl}${url}`, body, options)
      .pipe(
        map(this.extractData),
        catchError(this.handleError)
      );
  }

  public put<T>(url: string, body: any, options?: any, withTokenHeader = false): Observable<T> {
    const headers = withTokenHeader ? this.request.getBearerHeader() : this.request.getJsonHeader();
    options = { headers, ...options };

    return this.http.put(`${this.apiUrl}${url}`, body, options)
      .pipe(
        map(this.extractData),
        catchError(e => throwError(e))
      );
  }

  private extractData(res): any  {
    try {
      return res.json();
    } catch (e) {
      return res;
    }
  }

  private handleError(error: HttpErrorResponse) {
    console.error('Ha ocurrido un error', error);
    let msgError;

    if (error.error && error.error.message) {
      msgError = error.error.message;
    } else if (error.error) {
      msgError = error.error;
    } else {
      msgError = error;
    }

    return throwError({message: msgError, status: error.status});

  }

}
